package fingerling.zajecia3Tablice;

public class Zadanie1 {
//1. Napisac program, który:
//• utworzy tablice 10 liczb całkowitych i wypełni ja wartosciami losowymi z przedziału [−10, . . . , 10],
//• wypisze na ekranie zawartosc tablicy,
//• wyznaczy najmniejszy oraz najwieszy element w tablicy,
//• wyznaczy srednia arytmetyczna elementów tablicy,
//• wyznaczy ile elementów jest mniejszych, ile wiekszych od sredniej.
//• wypisze na ekranie zawartosc tablicy w odwrotnej kolejnosci, tj. od ostatniego do pierwszego.
//Wszystkie wyznaczone wartosci powinny zostac wyswietlone na ekranie. Wylosowane liczby:
//-3 9 2 -10 -3 -4 -1 -5 -10 8
//Min: -10, max: 9, Srednia: -1,00, Mniejszych od sr.: 6
//Wiekszych od sr.: 3, Liczby w odwrotnej kolejnosci: 8 -10 -5 -1 -4 -3 -10 2 9 -3

    public static void main(String[] args) {

        int[] tablica = new int[10];
        for (int i = 0; i < tablica.length; i++) {
            for (int y = -10; y <= 10; y++)
                System.out.println(tablica[i] = y);
        }

    }
}
