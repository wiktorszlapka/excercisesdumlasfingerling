package fingerling;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter first number: ");
        int firstNumber = scanner.nextInt();

        System.out.print("Enter second number: ");
        int secondNumber = scanner.nextInt();

        int result = sum(firstNumber, secondNumber);
        System.out.print("Result: " + result);

    }

    private static int sum(int a, int b) {
        return a + b;
    }

}
