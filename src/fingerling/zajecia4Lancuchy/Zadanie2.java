package fingerling.zajecia4Lancuchy;

import java.util.Scanner;

public class Zadanie2 {
    /*Napisac program, który wczytuje od uzytkownika ciag znaków, a nastepnie two-
rzy łancuch bedacy odwróceniem podanego łancucha i wyswietla go na ekranie.
Przykładowo, dla łancucha „Kot” wynikiem powinien byc łancuch „toK”.*/

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String wordToReverese = scanner.nextLine();

        System.out.println("Your word is " + wordToReverese);

        String reverse = "";
        for (int i = wordToReverese.length() -1; i >=0; i--) {
            reverse += wordToReverese.charAt(i);
        }
        System.out.println(reverse);
    }
}
