package fingerling.zajecia4Lancuchy;

import java.util.Scanner;

public class Zadanie1 {
/*Napisac program, który wczytuje od uzytkownika ciag znaków, a nastepnie wy-
swietla informacje o tym ile razy w tym ciagu powtarza sie jego ostatni znak.
Przykład, dla ciagu „Abrakadabra” program powinien wyswietlic 4, poniewaz
ostatnim znakiem jest literka „a”, która wystepuje w podanym ciagu łacznie 4
razy.
*/
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        String smallLetterFromWord = word.toLowerCase();
        char lastCharacter = smallLetterFromWord.charAt(smallLetterFromWord.length()-1);
        System.out.println("Last character is " + lastCharacter);

        int characterCounter = 0;

        for (int i =0; i < smallLetterFromWord.length(); i++) {
            char c = smallLetterFromWord.charAt(i);
            if (c == lastCharacter) {
                characterCounter++;

            }
            }
        System.out.println("In your word " + word + " last character is " + lastCharacter
        + "." + " You have " + characterCounter + " of " + lastCharacter + " in the word");

    }
}