package fingerling.zajecia1;

import java.util.Scanner;

public class zadanie5 {
    //    W sklepie ze sprzętem AGD oferowana jest sprzedaż ratalna. Napisz program
//    umożliwiający wyliczenie wysokości miesięcznej raty za zakupiony sprzęt. Danymi wejściowymi dla programu są:
//    cena towaru (od 100 zł do 10 tyś. zł),
//    liczba rat (od 6 do 48).
//    Kredyt jest oprocentowany w zależności od liczby rat:
//    od 6–12 wynosi 2.5% ,
//    od 13–24 wynosi 5%,
//    od 25–48 wynosi 10%.
//    Obliczona miesięczna rata powinna zawierać również odsetki. Program powinien
//    sprawdzać, czy podane dane mieszczą się w określonych powyżej zakresach, a w
//    przypadku błędu pytać prosić użytkownika ponownie o podanie danych.
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Cena towaru");
        double assortmentPrice = scanner.nextDouble();
            if (assortmentPrice >= 100 && assortmentPrice <= 10000) {
                System.out.println("Ilośc rat");
                int theAmountOfLoanInstallments = scanner.nextInt();

                if (theAmountOfLoanInstallments >= 6 && theAmountOfLoanInstallments <= 12) {
                    double loanValueForTwoAndHalfPercent = assortmentPrice + assortmentPrice * 0.025;
                    System.out.println("Wartość kredytu: " + loanValueForTwoAndHalfPercent);
                } else if (theAmountOfLoanInstallments > 6 && theAmountOfLoanInstallments <= 24) {
                    double loanValueForFivePercent = assortmentPrice + assortmentPrice * 0.05;
                    System.out.println("Wartość kredytu: " + loanValueForFivePercent);
                } else if (theAmountOfLoanInstallments > 24 && theAmountOfLoanInstallments <= 48) {
                    double loanValueForTenPercent = assortmentPrice + assortmentPrice * 0.1;
                    System.out.println("Wartość kredytu: " + loanValueForTenPercent);
                } else {
                    System.out.println("nie dostępna liczba rat");
                }
            } else {
                System.out.println("nie właściwa kwota kredytu");
            }
    }
}