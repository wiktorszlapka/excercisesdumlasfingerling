package fingerling.zajecia1;

import java.util.Scanner;

import static java.lang.Math.pow;

public class zadanie3 {
//Napisać program, który oblicza wartość współczynnika BMI (ang. body mass
//index) wg. wzoru: waga/wzrost^2
//Jeżeli wynik jest w przedziale (18,5 - 24,9) to wypisuje
//”waga prawidłowa”, jeżeli poniżej to ”niedowaga”, jeżeli powyżej ”nadwaga”.

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wagę");
        double weight = scanner.nextDouble();
        System.out.println("podaj wzrost w centymetrach");
        double high = scanner.nextDouble();

        double BMI = weight / (pow(high/100, 2));

        System.out.println(BMI);
        if (BMI >= 18.5 && BMI <= 24.9) {
            System.out.println("waga prawidłowa");
        } else if (BMI < 18.5) {
            System.out.println("zacznij żryć");
        } else if (BMI > 24.9) {
            System.out.println("Schudnij lebero");
        }

    }
}
