package fingerling.zajecia1;

import java.util.Scanner;

public class zadanie1 {
    //        Napisać program służący do konwersji wartości temperatury podanej w stopniach
//        Celsjusza na stopnie w skali Fahrenheita (stopnie Fahrenheita = 1.8 * stopnie
//        Celsjusza + 32.0)
    public static void main(String[] args) {

        System.out.println("Give me temperature in degrees Celsius");
        Scanner scanner = new Scanner(System.in);

        double fahrenheitScale = scanner.nextDouble() * 1.8 + 32;
        System.out.println("The fahrenheit temperature is " + fahrenheitScale);

    }
}
