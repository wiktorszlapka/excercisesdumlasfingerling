package fingerling.zajecia1;

import java.util.Scanner;

public class zadanie6 {
//Napisać program realizujący funkcje prostego kalkulatora, pozwalającego na wykonywanie operacji
//dodawania, odejmowania, mnożenia i dzielenia na dwóch liczbach rzeczywistych.
//Program ma identyfikować sytuację wprowadzenia błędnego symbolu działania oraz próbę dzielenia przez zero.
//Zastosować instrukcję switch do wykonania odpowiedniego działania w zależności od wprowadzonego symbolu
//operacji. Scenariusz działania programu:
//    a) Program wyświetla informację o swoim przeznaczeniu.
//    b) Wczytuje pierwszą liczbę.
//    c) Wczytuje symbol operacji arytmetycznej: +, -, *, /.
//    d) Wczytuje drugą liczbę.
//    e) Wyświetla wynik lub - w razie konieczności - informację o niemożności wykonania działania.
//    f) Program kończy swoje działanie po naciśnięciu przez użytkownika klawisza

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("X");
        double x = scanner.nextDouble();
        System.out.println("Y");
        double y = scanner.nextDouble();
        System.out.println("1 - dodawnie, 2 - odejmowanie, 3 - mnożenie, 4 - dzielenie");
        int toSwitch = scanner.nextInt();

        switch (toSwitch) {
            case 1:
                System.out.println(x + y);
                break;
            case 2:
                System.out.println(x - y);
                break;
            case 3:
                System.out.println(x * y);
                break;
            case 4:
                if (y == 0) {
                    System.out.println("Nie dziel przez 0");
                } else {
                    System.out.println(x / y);
                }
                break;
            default:
                System.out.println("nie wiem o co Ci chodzi");
                break;
        }
    }
}
