package fingerling.zajecia1;

import java.util.Scanner;

public class zadanie4 {
//    Napisać program obliczający należny podatek dochodowy od osób fizycznych. Pro-
//    gram ma pobierać od użytkownika dochód i po obliczeniu wypisywać na ekranie
//    należny podatek. Podatek obliczany jest wg. następujących reguł:
//    do 85.528 podatek wynosi 18% podstawy minus 556,02 PLN,
//    od 85.528 podatek wynosi 14.839,02 zł + 32% nadwyżki ponad 85.528,00

    public static void main(String[] args) {
        System.out.println("Podaj wartość przychodu");
        Scanner scanner = new Scanner(System.in);
        double incomeValue = scanner.nextDouble();

        if (incomeValue <= 85528) {
            double taxValueUnderLimit = incomeValue * 0.18 - 556.02;
            System.out.println("Your tax is:"+ taxValueUnderLimit);
        } else if  (incomeValue > 85528) {
            double taxValueOverLimit = 14839.02 + (incomeValue - 85528) * 0.32;
            System.out.println("Your tax is: "+ taxValueOverLimit);
        } else {
            System.out.println("Something go wrong");
        }
        }
    }

