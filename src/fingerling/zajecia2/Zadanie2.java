package fingerling.zajecia2;

import java.util.Scanner;

public class Zadanie2 {
//Napisać program pobierający od użytkownika dwie liczby całkowite
//A oraz B, A<B, a następnie wyznaczający sumę ciągu liczb od A do B,
//czyli sumę ciągu (A,A+1,...,B). Obliczenia należy wykonać trzykrotnie stosując kolejno pętle:
//for, while, do while, dla A = 4, B = 11: 60,60,60.

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();

        int suma = 0;
            if (a < b) {
                for (int i = a; i <= b; i++) {
                    suma += i;
                }
                System.out.println(suma);
            } else {
                System.out.println("Błędna wartość");
            }
        }
    }

