package fingerling.zajecia2;

import java.util.Scanner;

public class Zadanie1 {
//Napisać program, który pobiera od użytkownika liczbę całkowitą dodatnią, a następnie
//wyświetla na ekranie kolejno wszystkie liczby niepatrzyste nie większe od
//podanej liczby. Przykład, dla 15 program powinien wyświetlić 1, 3, 5, 7, 9, 11, 13,15.

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int scannerValue = scanner.nextInt();
        for (int i = 0; i <= scannerValue; i++) {
            if (i % 2 != 0) {
                System.out.println(i);
            }
        }
    }
}
